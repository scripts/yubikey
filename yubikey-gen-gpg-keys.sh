#!/usr/bin/env bash

# Set constants
readonly RAMDISK=/mnt/ramdisk
readonly CONTROLDIR="${RAMDISK}/ctrl"
readonly LOGDIR="${RAMDISK}/log"
readonly LOGFILE="${LOGDIR}/$(date +%s)_yubikey.log"


# Read parameters
GPGREALNAME="${1}"
GPGCOMMENT="${2}"
GPGEMAIL="${3}"
BACKUPPASSPHRASE="${4}"
BACKUPDIR="${5}"
EMAILTWO="${6}"

# If one variable is missed - stop here

if [ -z "${GPGREALNAME}" ] || [ -z "${GPGCOMMENT}" ] || [ -z "${GPGEMAIL}" ] || [ -z "${BACKUPPASSPHRASE}" ] || [ -z "${BACKUPDIR}" ]
then
    echo "usage: $0 GPGREALNAME GPGCOMMENT GPGEMAIL BACKUPPASSPHRASE BACKUPDIR [EMAILTWO]"
    exit 1
fi

# Check parameters

if [ ${#GPGREALNAME} -lt 5 ]
then
    echo "Error: GPGREALNAME must be at least 5 characters long!"
    exit 1
fi

if [ ${#BACKUPPASSPHRASE} -lt 12 ]
then
    echo "Error: BACKUPPASSPHRASE must be at least 12 characters long!"
    exit 1
fi

if [ ! -d "${BACKUPDIR}" ]
then
    echo "Error: ${BACKUPDIR} does not exists!"
    exit 1
fi

if [ ! -x "${BACKUPDIR}" ]
then
    echo "Error: ${BACKUPDIR} is not accessible!"
    exit 1
fi

## Create ramdisk for clean temporary GNUPGHOME

sudo mkdir -p "${RAMDISK}"
sudo mount -t ramfs ramfs ${RAMDISK}
sudo chown ${UID} ${RAMDISK}
mkdir -p ${CONTROLDIR}
mkdir -p ${LOGDIR}

# Setup new GNUPGHOME
export GNUPGHOME=$(mktemp -d --tmpdir=${RAMDISK});
export GPG_TTY=$(tty)

chmod 700 "${GNUPGHOME}"

cat << EOF > "${GNUPGHOME}/gpg.conf"
personal-cipher-preferences AES256 AES192 AES
personal-digest-preferences SHA512 SHA384 SHA256
personal-compress-preferences ZLIB BZIP2 ZIP Uncompressed
default-preference-list SHA512 SHA384 SHA256 AES256 AES192 AES ZLIB BZIP2 ZIP Uncompressed
cert-digest-algo SHA512
s2k-digest-algo SHA512
s2k-cipher-algo AES256
charset utf-8
fixed-list-mode
no-comments
no-emit-version
no-greeting
keyid-format 0xlong
list-options show-uid-validity
verify-options show-uid-validity
with-fingerprint
require-cross-certification
no-symkey-cache
use-agent
throw-keyids
EOF

chmod 600 ${GNUPGHOME}/gpg.conf

cat << EOF > "${GNUPGHOME}/gpg-agent.conf"
allow-loopback-pinentry
default-cache-ttl 600
max-cache-ttl 6000
EOF

chmod 600 ${GNUPGHOME}/gpg-agent.conf

#####
# Check Yubikey
#####

# echo
# echo 'Please reconnect your Yubikey and press any key'
# read

NUMYK=$(ykman list | wc -l)

if [ $NUMYK -eq 0 ]
then
    echo "Error: No Yubikey found!"
    exit 1
fi

if [ $NUMYK -gt 1 ]
then
    echo "Error: More than one Yubikey connected!"
    exit 1
fi

echo "Found Yubikey:"
ykman list
echo
echo "ALL DATA ON THIS YUBIKEY WILL BE ERASED!"
read -p "Are you sure? [y/N] "
if [[ ! $REPLY =~ ^[Yy] ]]
then
    echo "Error: User cancellation!"
    exit 1
fi

#####
## Reset Yubikey
#####

# Reset code for yubikey
echo "Wipe the yubikey..." | tee -a "${LOGFILE}"

cat << EOF > "${CONTROLDIR}/reset.yubi"

/echo Yubikey reset input comes from https://gist.github.com/pkirkovsky/c3d703633effbdfcb48c
/hex
scd serialno
scd apdu 00 20 00 81 08 40 40 40 40 40 40 40 40
scd apdu 00 20 00 81 08 40 40 40 40 40 40 40 40
scd apdu 00 20 00 81 08 40 40 40 40 40 40 40 40
scd apdu 00 20 00 81 08 40 40 40 40 40 40 40 40
scd apdu 00 20 00 83 08 40 40 40 40 40 40 40 40
scd apdu 00 20 00 83 08 40 40 40 40 40 40 40 40
scd apdu 00 20 00 83 08 40 40 40 40 40 40 40 40
scd apdu 00 20 00 83 08 40 40 40 40 40 40 40 40
scd apdu 00 e6 00 00
scd apdu 00 44 00 00
/echo Yubikey has been successfully reset.
/echo The factory default PINs are 123456 (user) and 12345678 (admin).
EOF

# Reset Yubikey
gpg-connect-agent < "${CONTROLDIR}/reset.yubi"  >> "${LOGFILE}" 2>&1

if [ $? -ne 0 ]
then
    echo "Error: Wiping the Yubikey failed!"
    echo "Logfile: ${LOGFILE}"
    exit 1
fi

#####
## Setup the key attributes on Yubikey
#####

# Input for setting key attributes
cat << EOF > "${CONTROLDIR}/keyattr.yubi"
admin
key-attr
1
4096
1
4096
1
4096
quit
EOF

echo "Setup key attributes to 4096 bits..." | tee -a "${LOGFILE}"
# set key attributes
cat "${CONTROLDIR}/keyattr.yubi" | gpg --no-tty --command-fd=0 --status-fd=1 --card-edit --expert --pinentry-mode loopback --passphrase "12345678" >> "${LOGFILE}" 2>&1

if [ $? -ne 0 ]
then
    echo "Error: Setup key attributes failed!"
    echo "Logfile: ${LOGFILE}"
    exit 1
fi


#####
## Generate GPG keys
#####

# Generates the answer file for the master key
cat << EOF > "${CONTROLDIR}/keygen.master"
8
s
e
q
4096
0
EOF
echo "${GPGREALNAME}" >> "${CONTROLDIR}/keygen.master"
echo "${GPGEMAIL}" >> "${CONTROLDIR}/keygen.master"
echo "${GPGCOMMENT}" >> "${CONTROLDIR}/keygen.master"

# Generates the master key
echo "Generating the master key..." | tee -a "${LOGFILE}"
cat "${CONTROLDIR}/keygen.master" | gpg --no-tty --command-fd=0 --status-fd=1 --expert --full-gen-key --pinentry-mode loopback --passphrase '' >> "${LOGFILE}" 2>&1

if [ $? -ne 0 ]
then
    echo "Error: Generating the master key failed!"
    echo "Logfile: ${LOGFILE}"
    exit 1
fi

# Get the fingerprint an ID
KEYFPR=$(gpg --list-options show-only-fpr-mbox --list-secret-keys 2>/dev/null | awk '{print $1}' )
KEYID=$(gpg -K --with-colons 2>/dev/null | tac | grep -i -m1 "sec" | cut -d ":" -f5 )

# Add a second email if provided

if [ ! -z "${EMAILTWO}" ]
then

    cat << EOF > "${CONTROLDIR}/adduid.master"
adduid
${GPGREALNAME}
${EMAILTWO}
${GPGCOMMENT}
O
uid 2
trust
5
y
save
EOF

    echo "Adding 2nd email..." | tee -a "${LOGFILE}"
    cat "${CONTROLDIR}/adduid.master" | gpg --no-tty --command-fd=0 --status-fd=1 --expert --edit-key --pinentry-mode loopback --passphrase '' "${KEYID}" >> "${LOGFILE}" 2>&1

    if [ $? -ne 0 ]
    then
        echo "Error: Adding 2nd email failed!"
        echo "Logfile: ${LOGFILE}"
        exit 1
    fi

fi

# Generates the answer file for the sub key generation
cat << EOF > "${CONTROLDIR}/keygen.sub"
addkey
8
s
q
4096
365
addkey
8
e
q
4096
365
addkey
8
e
s
a
q
4096
365
save
EOF

# Generates the subkeys
echo "Generating the subkeys..." | tee -a "${LOGFILE}"
cat "${CONTROLDIR}/keygen.sub" | gpg --no-tty --command-fd=0 --status-fd=1 --expert --edit-key --pinentry-mode loopback --passphrase '' "${KEYID}" >> "${LOGFILE}" 2>&1

if [ $? -ne 0 ]
then
    echo "Error: Generating the subkeys failed!"
    echo "Logfile: ${LOGFILE}"
    exit 1
fi

#####
# Create Backup
#####

# Create a temp backup dir
mkdir -p "${RAMDISK}/${KEYID}"

echo "Exporting keys for backup..." | tee -a "${LOGFILE}"
# Backup public master key
gpg --export --armor --pinentry-mode loopback --passphrase '' "${KEYID}" > "${RAMDISK}/${KEYID}/${KEYID}.pub.asc" 2>>"${LOGFILE}"

if [ $? -ne 0 ]
then
    echo "Error: Backup public master key failed!"
    echo "Logfile: ${LOGFILE}"
    exit 1
fi

# Backup private master key
gpg --export-secret-keys --armor --pinentry-mode loopback --passphrase '' "${KEYID}" > "${RAMDISK}/${KEYID}/${KEYID}.sec.asc" 2>>"${LOGFILE}"

if [ $? -ne 0 ]
then
    echo "Error: Backup private master key failed!"
    echo "Logfile: ${LOGFILE}"
    exit 1
fi

# Backup sub keys
gpg --export-secret-subkeys --armor --pinentry-mode loopback --passphrase '' "${KEYID}" > "${RAMDISK}/${KEYID}/${KEYID}.sub.sec.asc" 2>>"${LOGFILE}"

if [ $? -ne 0 ]
then
    echo "Error: Backup sub keys failed!"
    echo "Logfile: ${LOGFILE}"
    exit 1
fi

# Generating paperkey backup if available
# https://wiki.archlinux.org/index.php/Paperkey
if command -v paperkey &> /dev/null
then
    gpg --export-secret-key --pinentry-mode loopback --passphrase '' "${KEYID}" | paperkey --output "${RAMDISK}/${KEYID}/${KEYID}.paperkey.asc"

    if [ $? -ne 0 ]
    then
        echo "Error: Paperkey backup failed!"
        echo "Logfile: ${LOGFILE}"
        exit 1
    fi
fi


echo "Generating revocation certificate..."
# Create answer file for revocation certificate
cat << EOF > "${CONTROLDIR}/cert.rev"
y
0
Generic revocation certicicate

y
EOF

# Create revocation certificate
cat "${CONTROLDIR}/cert.rev" | gpg --no-tty --command-fd=0 --status-fd=1 --output "${RAMDISK}/${KEYID}/${KEYID}.rev.asc" --gen-revoke "${KEYID}" >> "${LOGFILE}" 2>&1
#gpg --output "${RAMDISK}/${KEYID}/${KEYID}.rev.asc" --gen-revoke "${KEYID}"
echo "Creating encrypted backup..." | tee -a "${LOGFILE}"
# Encrypting backup
mkdir -p "${BACKUPDIR}/${KEYID}"
tar -cj -C "${RAMDISK}" "${KEYID}" | gpg --output "${BACKUPDIR}/${KEYID}/${KEYID}.tar.bz2.gpg" --symmetric --pinentry-mode loopback  --passphrase "${BACKUPPASSPHRASE}"  >> "${LOGFILE}" 2>&1

if [ $? -ne 0 ]
then
    echo "Error: Creating encrypted backup failed!"
    echo "Logfile: ${LOGFILE}"
    exit 1
fi

# Copy public key to BACKUPDIR
cp "${RAMDISK}/${KEYID}/${KEYID}.pub.asc" "${BACKUPDIR}/${KEYID}/${KEYID}.txt"

# Copy revocation certificate to BACKUPDIR
cp "${RAMDISK}/${KEYID}/${KEYID}.rev.asc" "${BACKUPDIR}/${KEYID}/${KEYID}.rev"

# Delete unencrypted backup
rm -rf "${RAMDISK}/${KEYID}"

#####
## Transfer the keys to the Yubikey
#####

# Input for transfering keys to yubikey
cat << EOF > "${CONTROLDIR}/key2yubi.yubi"
key 1
keytocard
2
key 1
key 2
keytocard
1
key 2
key 3
keytocard
3
key 3
save
EOF

echo "Transferring keys to Yubikey..." | tee -a "${LOGFILE}"
TRY=1

echo
echo 'Please reconnect your Yubikey and press any key'
read

# Transfer keys to yubikey
cat "${CONTROLDIR}/key2yubi.yubi" | gpg --no-tty --command-fd=0 --status-fd=1 --pinentry-mode loopback --passphrase "12345678" --edit-key --expert "${KEYID}"  >> "${LOGFILE}" 2>&1

if [ $? -ne 0 ]
then
    echo "Error: Transferring keys to Yubikey failed!"
    echo "Logfile: ${LOGFILE}"
    exit 1
fi

echo "Setup successful!" | tee -a "${LOGFILE}"

# Unmount Ramdisk
sudo umount "${RAMDISK}"

# Show results
echo "Your key id: ${KEYID}"
echo "Your key fingerprint: ${KEYFPR}"
echo "Your public key: ${BACKUPDIR}/${KEYID}/${KEYID}.txt"
echo "Backup can be found here: ${BACKUPDIR}/${KEYID}/${KEYID}.tar.bz2.gpg"
echo "Revocation certificate: ${BACKUPDIR}/${KEYID}/${KEYID}.rev"
echo "Logfile: ${LOGFILE}"