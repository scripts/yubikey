# Scripts for handling and provisioning Yubikeys

## Generate new OpenGPG-Keys and transfer them to the yubikey

Script: yubikey-gen-gpg-keys.sh

Needed packages:

```
  - cryptsetup
  - curl
  - dirmngr
  - gnupg2
  - gnupg-agent
  - hopenpgp-tools
  - paperkey
  - pcscd
  - python3-setuptools
  - scdaemon
  - secure-delete
  - tmux
  - yubikey-manager
  - yubikey-personalization
```

For copy & paste: `sudo apt install cryptsetup curl dirmngr gnupg2 gnupg-agent hopenpgp-tools paperkey pcscd python3-setuptools scdaemon secure-delete tmux yubikey-manager yubikey-personalization`

Usage:

```
/bin/bash yubikey-gen-gpg-keys.sh GPGREALNAME GPGCOMMENT GPGEMAIL BACKUPPASSPHRASE BACKUPDIR [EMAILTWO]
```

Example:

```
/bin/bash yubikey-gen-gpg-keys.sh 'Oliver Heller' 'YK 7593206' 'heller@mis.mpg.de' 'Super$uperS3cure!' '/mnt/usbstick'
```

Example with a second email address:

```
/bin/bash yubikey-gen-gpg-keys.sh 'Oliver Heller' 'YK 7593206' 'heller@mis.mpg.de' 'Super$uperS3cure!' '/mnt/usbstick' 'oliver.heller@mis.mpg.de'
```
